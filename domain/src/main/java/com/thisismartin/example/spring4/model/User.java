package com.thisismartin.example.spring4.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * @author Martin Spasovski (@moondowner)
 */
@Entity
@Table(name = "T_USER")
public class User extends SuperEntity {

	public User() {
	}

    @NotNull
	@Column(name = "email", length = 300, unique = true)
	private String email;

    @NotNull
	@Size(min = 3, max = 72)
	// the actual password can be max 72 chars for BCrypt
	@Column(name = "password", length = 60)
	// the actual BCrypt 'hash' is always 60 chars
	private String password;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("User [getEmail()=").append(getEmail())
				.append(", getPassword()=").append(getPassword())
				.append(", getUuid()=").append(getUuid()).append("]");
		return builder.toString();
	}

}
