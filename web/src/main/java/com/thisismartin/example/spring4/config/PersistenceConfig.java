package com.thisismartin.example.spring4.config;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

/**
 * @author Martin Spasovski (@moondowner)
 */
@Configuration
@EnableJpaRepositories(basePackages = Constants.REPOSITORY_PACKAGE)
@EnableTransactionManagement
public class PersistenceConfig {

	@Bean(destroyMethod = "close")
	public DataSource dataSource(Environment env) {
		HikariConfig dataSourceConfig = new HikariConfig();
		dataSourceConfig.setDriverClassName(env
				.getRequiredProperty(Constants.PROPERTY_NAME_DB_DRIVER_CLASS));
		dataSourceConfig.setJdbcUrl(env
				.getRequiredProperty(Constants.PROPERTY_NAME_DB_URL));
		dataSourceConfig.setUsername(env
				.getRequiredProperty(Constants.PROPERTY_NAME_DB_USER));
		dataSourceConfig.setPassword(env
				.getRequiredProperty(Constants.PROPERTY_NAME_DB_PASSWORD));

		return new HikariDataSource(dataSourceConfig);
	}

	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory(
			DataSource dataSource, Environment env) {
		LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
		entityManagerFactoryBean.setDataSource(dataSource);
		entityManagerFactoryBean
				.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
		entityManagerFactoryBean.setPackagesToScan(Constants.ENTITY_PACKAGE);

		Properties jpaProperties = new Properties();

		// Configures the used database dialect. This allows Hibernate to create
		// SQL
		// that is optimized for the used database.
		jpaProperties
				.put(Constants.PROPERTY_NAME_HIBERNATE_DIALECT,
						env.getRequiredProperty(Constants.PROPERTY_NAME_HIBERNATE_DIALECT));

		// Specifies the action that is invoked to the database when the
		// Hibernate
		// SessionFactory is created or closed.
		jpaProperties
				.put(Constants.PROPERTY_NAME_HIBERNATE_HBM2DDL_AUTO,
						env.getRequiredProperty(Constants.PROPERTY_NAME_HIBERNATE_HBM2DDL_AUTO));

		// Configures the naming strategy that is used when Hibernate creates
		// new database objects and schema elements
		jpaProperties
				.put(Constants.PROPERTY_NAME_HIBERNATE_NAMING_STRATEGY,
						env.getRequiredProperty(Constants.PROPERTY_NAME_HIBERNATE_NAMING_STRATEGY));

		// If the value of this property is true, Hibernate writes all SQL
		// statements to the console.
		jpaProperties
				.put(Constants.PROPERTY_NAME_HIBERNATE_SHOW_SQL,
						env.getRequiredProperty(Constants.PROPERTY_NAME_HIBERNATE_SHOW_SQL));

		// If the value of this property is true, Hibernate will use prettyprint
		// when it writes SQL to the console.
		jpaProperties
				.put(Constants.PROPERTY_NAME_HIBERNATE_FORMAT_SQL,
						env.getRequiredProperty(Constants.PROPERTY_NAME_HIBERNATE_FORMAT_SQL));

		entityManagerFactoryBean.setJpaProperties(jpaProperties);

		return entityManagerFactoryBean;
	}

	/**
	 * Creates the transaction manager bean that integrates the used JPA
	 * provider with the Spring transaction mechanism.
	 * 
	 * @param entityManagerFactory
	 *            The used JPA entity manager factory.
	 * @return
	 */
	@Bean
	public JpaTransactionManager transactionManager(
			EntityManagerFactory entityManagerFactory) {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(entityManagerFactory);
		return transactionManager;
	}
}
