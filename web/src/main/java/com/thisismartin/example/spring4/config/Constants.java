package com.thisismartin.example.spring4.config;

/**
 * @author Martin Spasovski (@moondowner)
 */
public final class Constants {

	private Constants() {
	}

	/**
	 * Profiles.
	 */
	public static final String SPRING_PROFILE_DEVELOPMENT = "dev";
	public static final String SPRING_PROFILE_PRODUCTION = "prod";

	/**
	 * Property keys.
	 */
	public static final String PROPERTY_NAME_DB_DRIVER_CLASS = "db.driver";
	public static final String PROPERTY_NAME_DB_PASSWORD = "db.password";
	public static final String PROPERTY_NAME_DB_URL = "db.url";
	public static final String PROPERTY_NAME_DB_USER = "db.username";

	public static final String PROPERTY_NAME_HIBERNATE_DIALECT = "hibernate.dialect";
	public static final String PROPERTY_NAME_HIBERNATE_FORMAT_SQL = "hibernate.format_sql";
	public static final String PROPERTY_NAME_HIBERNATE_HBM2DDL_AUTO = "hibernate.hbm2ddl.auto";
	public static final String PROPERTY_NAME_HIBERNATE_NAMING_STRATEGY = "hibernate.ejb.naming_strategy";
	public static final String PROPERTY_NAME_HIBERNATE_SHOW_SQL = "hibernate.show_sql";

	/**
	 * JPA model.
	 */
	public static final String ENTITY_PACKAGE = "com.thisismartin.example.spring4.model";
	
	/**
	 * Spring Data JPA Repos.
	 */
	public static final String REPOSITORY_PACKAGE = "com.thisismartin.example.spring4.repository";

}