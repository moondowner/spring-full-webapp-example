package com.thisismartin.example.spring4.controller.web;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.thisismartin.example.spring4.exception.UserAlreadyRegisteredException;
import com.thisismartin.example.spring4.model.User;
import com.thisismartin.example.spring4.service.UserService;

/**
 * @author Martin Spasovski (@moondowner)
 */
@Controller
public class RegisterController {

	private static final Log logger = LogFactory
			.getLog(RegisterController.class);

	@Resource
	private UserService userService;

	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String getRegisterPage(Model model) {

		model.addAttribute("user", new User());

		logger.info("Returning main page;");
		return "register";
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String createUser(@ModelAttribute User user)
			throws UserAlreadyRegisteredException {

		User exists = userService.readByEmail(user.getEmail());
		if (exists != null) {
			logger.info("Error: User is already registered");
			throw new UserAlreadyRegisteredException();
		}

		logger.info("Creating user: " + user);
		userService.create(user);

		logger.info("Redirecting to main;");
		return "redirect:/";
	}

}
