package com.thisismartin.example.spring4.controller.web;

import java.security.Principal;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.thisismartin.example.spring4.exception.TodoNotFoundException;
import com.thisismartin.example.spring4.service.TodoService;

/**
 * @author Martin Spasovski (@moondowner)
 */
@Controller
public class ListController {

	private static final Log logger = LogFactory.getLog(ListController.class);

	@Resource
	private TodoService todoService;

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public String searchList(
			Model model,
			@RequestParam(required = false, value = "search-param") String searchParameter,
			Principal principal) {

		if (searchParameter == null) {
			logger.info("Reading all todos;");
			model.addAttribute("todos", todoService.searchByUser(principal));

		} else {
			logger.info("Reading todos with query: " + searchParameter);
			model.addAttribute("todos", todoService.seachByUserAndParameter(
					principal, searchParameter.toUpperCase()));
		}

		logger.info("Returning main page;");
		return "list";
	}

	@RequestMapping(value = "/list/{id}/delete", method = RequestMethod.GET)
	public String deleteTodo(Model model, @PathVariable(value = "id") UUID id,
			Principal principal) throws TodoNotFoundException {

		logger.info("Deleting TODO with id:" + id);
		todoService.delete(id);

		logger.info("Reading all todos;");
		model.addAttribute("todos", todoService.searchByUser(principal));

		logger.info("Returning main page;");
		return "redirect:/list";
	}

	@RequestMapping(value = "/list/{id}/complete", method = RequestMethod.GET)
	public String completeTodo(Model model,
			@PathVariable(value = "id") UUID id, Principal principal)
			throws TodoNotFoundException {

		logger.info("Marking as completed TODO with id:" + id);
		todoService.markAsCompleted(id);

		logger.info("Reading all todos;");
		model.addAttribute("todos", todoService.searchByUser(principal));

		logger.info("Returning main page;");
		return "redirect:/list";
	}

}
