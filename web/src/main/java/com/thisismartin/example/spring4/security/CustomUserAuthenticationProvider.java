package com.thisismartin.example.spring4.security;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.thisismartin.example.spring4.model.User;
import com.thisismartin.example.spring4.service.UserService;

/**
 * @author Martin Spasovski (@moondowner)
 */
@Component
public class CustomUserAuthenticationProvider implements AuthenticationProvider {

	private static final Log logger = LogFactory
			.getLog(CustomUserAuthenticationProvider.class);

	@Resource
	private UserService userService;

	@Resource
	private PasswordEncoder passwordEncoder;

	@Override
	public Authentication authenticate(Authentication authentication) {
		logger.info("Authenticating: " + authentication.getName());

		User user = userService.readByEmail(authentication.getName());

		if (user == null) {
			logger.info("Authentication not successful.");
			return null;
		}

		if (user.getEmail().equals(authentication.getName())
				&& passwordEncoder.matches(authentication.getCredentials()
						.toString(), user.getPassword())) {

			List<GrantedAuthority> grantedAuths = new ArrayList<>();
			grantedAuths.add(new SimpleGrantedAuthority("ROLE_USER"));

			Authentication auth = new UsernamePasswordAuthenticationToken(
					user.getEmail(), user.getPassword(), grantedAuths);
			return auth;
		}

		logger.info("Authentication not successful.");
		return null;
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return UsernamePasswordAuthenticationToken.class
				.isAssignableFrom(authentication);
	}
}