package com.thisismartin.example.spring4.config;

import java.util.EnumSet;

import javax.servlet.DispatcherType;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;
import org.springframework.security.web.session.HttpSessionEventPublisher;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.DelegatingFilterProxy;
import org.springframework.web.servlet.DispatcherServlet;

/**
 * @author Martin Spasovski (@moondowner)
 */
public class WebAppInitializer implements WebApplicationInitializer {

	// @Inject
	// private Environment env;

	public void onStartup(ServletContext container) throws ServletException {

		// http://docs.spring.io/spring/docs/4.0.0.RELEASE/javadoc-api/org/springframework/web/WebApplicationInitializer.html
		// Create the 'root' Spring application context
		AnnotationConfigWebApplicationContext rootContext = new AnnotationConfigWebApplicationContext();
		rootContext.register(WebConfig.class);
		rootContext.setDisplayName("Todo Manager");

		// Set active profiles
		rootContext.getEnvironment().setActiveProfiles(
				Constants.SPRING_PROFILE_DEVELOPMENT);
		// TODO like in jhipster

		// Add context to container
		container.addListener(new ContextLoaderListener(rootContext));
		
		// Spring Security: the user cannot login right after logout
		container.addListener(new HttpSessionEventPublisher());

		// Add security filter
		container
				.addFilter(
						AbstractSecurityWebApplicationInitializer.DEFAULT_FILTER_NAME,
						new DelegatingFilterProxy(
								AbstractSecurityWebApplicationInitializer.DEFAULT_FILTER_NAME))
				.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class),
						false, "/*");

		// Register and map the dispatcher servlet
		ServletRegistration.Dynamic dispatcher = container.addServlet(
				"dispatcher", new DispatcherServlet(rootContext));
		dispatcher.setLoadOnStartup(1);
		dispatcher.addMapping("/");
	}
}