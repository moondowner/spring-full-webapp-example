package com.thisismartin.example.spring4.service;

import java.util.UUID;

import javax.annotation.Resource;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thisismartin.example.spring4.model.User;
import com.thisismartin.example.spring4.repository.UserRepository;

/**
 * @author Martin Spasovski (@moondowner)
 */
@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {

	@Resource
	private UserRepository userRepository;

	@Resource
	private PasswordEncoder passwordEncoder;

	@Override
	public User create(User dto) {
		dto.setPassword(passwordEncoder.encode(dto.getPassword()));
		return userRepository.save(dto);
	}

	@Override
	public User search(UUID id) {
		return userRepository.findOne(id);
	}

	@Override
	public User update(User dto) {
		return userRepository.save(dto);
	}

	@Override
	public void delete(UUID id) {
		userRepository.delete(id);
	}

	@Override
	public User readByEmail(String userEmail) {
		return userRepository.findByEmail(userEmail);
	}

}
