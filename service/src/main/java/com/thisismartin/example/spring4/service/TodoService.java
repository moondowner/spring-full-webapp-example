package com.thisismartin.example.spring4.service;

import java.security.Principal;
import java.util.UUID;

import com.thisismartin.example.spring4.exception.TodoNotFoundException;
import com.thisismartin.example.spring4.model.Todo;

/**
 * @author Martin Spasovski (@moondowner)
 */
public interface TodoService {

	//
	// the crud stuff
	//

	Todo create(Todo todo);

	Todo search(UUID id) throws TodoNotFoundException;

	Todo update(Todo todo);

	void delete(UUID id) throws TodoNotFoundException;

	//
	// the custom stuff
	//

	Iterable<Todo> searchByUser(Principal principal);

	/**
	 * @param searchParameter should be uppercase.
	 */
	Iterable<Todo> seachByUserAndParameter(Principal principal,
			String searchParameter);

	Todo create(Todo dto, Principal principal);

	Todo markAsCompleted(UUID id) throws TodoNotFoundException;

}
