package com.thisismartin.example.spring4.exception;

/**
 * @author Martin Spasovski (@moondowner)
 */
public class UserAlreadyRegisteredException extends Exception {

	private static final long serialVersionUID = 2284944347261890384L;

}
