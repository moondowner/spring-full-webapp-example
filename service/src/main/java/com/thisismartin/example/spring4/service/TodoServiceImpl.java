package com.thisismartin.example.spring4.service;

import java.security.Principal;
import java.util.UUID;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thisismartin.example.spring4.exception.TodoNotFoundException;
import com.thisismartin.example.spring4.model.Todo;
import com.thisismartin.example.spring4.repository.TodoRepository;

/**
 * @author Martin Spasovski (@moondowner)
 */
@Service("todoService")
@Transactional
public class TodoServiceImpl implements TodoService {

	@Resource
	private TodoRepository todoRepository;

	@Override
	public Todo create(Todo todo) {
		return todoRepository.save(todo);
	}

	@Override
	public Todo search(UUID id) throws TodoNotFoundException {
		Todo found = todoRepository.findOne(id);

		if (found == null) {
			throw new TodoNotFoundException();
		}

		return found;
	}

	@Override
	public Todo update(Todo todo) {
		return todoRepository.save(todo);
	}

	@Override
	public void delete(UUID id) throws TodoNotFoundException {
		Todo found = search(id);

		if (found == null) {
			throw new TodoNotFoundException();
		}

		delete(found);
	}

	private void delete(Todo todo) {
		todoRepository.delete(todo);
	}

	@Override
	public Todo create(Todo dto, Principal principal) {
		dto.setEmail(principal.getName());
		return create(dto);
	}

	@Override
	public Todo markAsCompleted(UUID id) throws TodoNotFoundException {
		Todo found = search(id);

		if (found == null) {
			throw new TodoNotFoundException();
		}

		found.setCompleted(true);
		return update(found);
	}

	@Override
	public Iterable<Todo> searchByUser(Principal principal) {
		return seachByUserAndParameter(principal, "%");
	}

	@Override
	public Iterable<Todo> seachByUserAndParameter(Principal principal,
			String searchParameter) {
		return todoRepository.findByEmailAndTitleLike(principal.getName(),
				searchParameter);
	}

}
