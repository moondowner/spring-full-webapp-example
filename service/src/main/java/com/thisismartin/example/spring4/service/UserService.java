package com.thisismartin.example.spring4.service;

import java.util.UUID;

import com.thisismartin.example.spring4.model.User;

/**
 * @author Martin Spasovski (@moondowner)
 */
public interface UserService {

	//
	// the crud stuff
	//

	User create(User dto);

	User search(UUID id);

	User update(User dto);

	void delete(UUID id);

	//
	// the custom stuff
	//

	User readByEmail(String userEmail);
}
